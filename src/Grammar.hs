module Grammar (Grammar(..), Structure(..), fillGram) where

import Data.Maybe

data Structure symb goal
  = Symb symb
  | Filled goal
  | Connect ([ goal ] -> goal) [ Structure symb goal ]

data Grammar symb goal = Grammar
  { startSymbol :: symb
  , fillRule    :: symb -> [ [ Structure symb goal ] ]
  }

fillGram :: (Num n, Eq n) => Grammar symb goal -> n -> [ [ goal ] ]
fillGram gram = go [ [ Symb (startSymbol gram) ] ] gram
  where
    go :: (Num n, Eq n) => [ [ Structure symb goal ] ] -> Grammar symb goal -> n -> [ [ goal ] ]
    go workSpace gram 0 = mapMaybe (mSwap collectStructure) workSpace
    go workSpace gram n = go (fillNTS gram =<< workSpace) gram (n - 1)

fillNTS :: Grammar symb goal -> [ Structure symb goal ] -> [ [ Structure symb goal ] ]
fillNTS gram sentence = either id pure $ go [] gram sentence
  where
    go  :: [ Structure symb goal ] -> Grammar symb goal -> [ Structure symb goal ] -> Either [ [ Structure symb goal ] ] [ Structure symb goal]
    go prev gram [] = Right $ reverse prev
    go prev gram (Symb nts : rest)
      = Left $ ( (reverse prev ++) . (++ rest) ) <$> fillRule gram nts
    go prev gram (now@(Filled _) : rest) = go (now : prev) gram rest
    go prev gram ((Connect builder sentence) : rest) =
      case go [] gram sentence of
        Right sentence    ->
          go (Connect builder sentence : prev) gram rest
        Left newSentences ->
          Left $ ( (reverse prev ++) . (: rest) . Connect builder ) <$> newSentences

terminal :: Structure g s -> Bool
terminal (Symb _) = False
terminal (Filled _) = True
terminal (Connect _ structs) = all terminal structs

collectStructure :: Structure s g -> Maybe g
collectStructure (Symb _)      = Nothing
collectStructure (Filled goal) = Just goal
collectStructure (Connect builder contents) = builder <$> mSwap collectStructure contents

mSwap :: (b -> Maybe a) -> [b] -> Maybe [a]
mSwap = go (Just [])
  where
    go :: Maybe [a] -> (b -> Maybe a) -> [b] -> Maybe [a]
    go res f [] = reverse <$> res
    go res f (b : bs) =
      case f b of
        Just a  -> go ((a :) <$> res) f bs
        Nothing -> Nothing
