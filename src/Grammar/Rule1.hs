module Grammar.Rule1 (gram1) where

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn

data Symbol = Symbol {
    _inside :: BFToken
  , _after  :: BFToken
  }

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Symbol a _) = [
    [ Filled Height                                                           ]
  , [ Filled Swap                                                             ]
  , [ Filled Pop                                                              ]
  , [ Filled Unit                                                             ]
  , [ Filled Height                               , Symb $ Symbol a HeightTkn ]
  , [ Filled Swap                                 , Symb $ Symbol a SwapTkn   ]
  , [ Filled Pop                                  , Symb $ Symbol a PopTkn    ]
  , [ Filled Unit                                 , Symb $ Symbol a UnitTkn   ]
  , [ Connect Push [ Symb $ Symbol PushTkn NoTkn ]                            ]
  , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn ]                            ]
  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn ]                            ]
  , [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn ]                            ]
  , [ Connect Push [ Symb $ Symbol PushTkn NoTkn ], Symb $ Symbol a PushTkn   ]
  , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn ], Symb $ Symbol a ZeroTkn   ]
  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn ], Symb $ Symbol a NegaTkn   ]
  , [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn ], Symb $ Symbol a LoopTkn   ]
  ]

gram1 = Grammar { startSymbol = Symbol NoTkn NoTkn, fillRule = rule }
