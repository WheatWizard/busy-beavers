module Grammar.Rule2 (gram2) where

{- Here we create a ``Double" scope to replace using (...){} for doubling -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken
  | Delay Symbol

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule (Symbol insideOf rightOf) = [
    [ Filled Height                                                                                      ]
  , [ Filled Swap                                                                                        ]
  , [ Filled Unit                                                                                        ]
  , [ Filled Height,                                                    Symb $ Symbol insideOf HeightTkn ]
  , [ Filled Swap  ,                                                    Symb $ Symbol insideOf SwapTkn   ]
  , [ Filled Unit  ,                                                    Symb $ Symbol insideOf UnitTkn   ]
  , [ Connect Push [ Symb $         Symbol PushTkn NoTkn ]                                               ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn ], Filled Pop                                   ]
  , [ Connect Zero [ Symb $         Symbol ZeroTkn NoTkn ]                                               ]
  , [ Connect Nega [ Symb $         Symbol NegaTkn NoTkn ]                                               ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn ]                                               ]
  , [ Connect Push [ Symb $         Symbol PushTkn NoTkn ],             Symb $ Symbol insideOf PushTkn   ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn ], Filled Pop, Symb $ Symbol insideOf DoubTkn   ]
  , [ Connect Zero [ Symb $         Symbol ZeroTkn NoTkn ],             Symb $ Symbol insideOf ZeroTkn   ]
  , [ Connect Nega [ Symb $         Symbol NegaTkn NoTkn ],             Symb $ Symbol insideOf NegaTkn   ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn ],             Symb $ Symbol insideOf LoopTkn   ]
  ] ++ pops
  where
    pops = if rightOf /= PushTkn -- Cannot have a pop after a push, use Doub instead
             then [
                 [ Filled Pop                                ]
               , [ Filled Pop, Symb $ Symbol insideOf PopTkn ]
               ]
              else []

gram2 = Grammar { startSymbol = Symbol NoTkn NoTkn, fillRule = rule }
