module Grammar.Rule3 (gram3) where

{- Here we make dedicated way to push 0 -}
{- We eliminate <><>, <..()..>, <..[]..>, <..<..>..> and <..[..]..> -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken
  | Delay Symbol
  | Val0

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule (Symbol insideOf rightOf) = [
    [ Connect Push [ Symb $         Symbol PushTkn NoTkn ]                                               ]
  , [ Connect Push [ Symb $ Delay $ Val0                 ]                                               ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn ], Filled Pop                                   ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn ]                                               ]
  , [ Connect Push [ Symb $         Symbol PushTkn NoTkn ],             Symb $ Symbol insideOf PushTkn   ]
  , [ Connect Push [ Symb $ Delay $ Val0                 ],             Symb $ Symbol insideOf Push0Tkn  ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn ], Filled Pop, Symb $ Symbol insideOf DoubTkn   ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn ],             Symb $ Symbol insideOf LoopTkn   ]
  ] ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    pops = if notElem rightOf [ PushTkn, Push0Tkn ] -- Cannot have a pop after a push, use Doub instead
             then [
                 [ Filled Pop                                ]
               , [ Filled Pop, Symb $ Symbol insideOf PopTkn ]
               ]
              else []
    swaps = if rightOf /= SwapTkn -- Cannot have a swap right after a swap
             then [
                 [ Filled Swap                                 ]
               , [ Filled Swap, Symb $ Symbol insideOf SwapTkn ]
               ]
              else []
    units = if insideOf /= ZeroTkn -- cannot have a unit inside of a zero
              then [
                  [ Filled Unit                                 ]
                , [ Filled Unit, Symb $ Symbol insideOf UnitTkn ]
                ]
               else []
    heights = if insideOf /= ZeroTkn -- cannot have a height inside of a zero
                then [
                    [ Filled Height                                   ]
                  , [ Filled Height, Symb $ Symbol insideOf HeightTkn ]
                  ]
                 else []
    negates = if insideOf /= ZeroTkn -- Cannot have a negate inside of a zero token
                then [
                    [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn ]                                 ]
                  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn ], Symb $ Symbol insideOf NegaTkn ]
                  ]
                else []
    zeros = if insideOf /= ZeroTkn -- Cannot have a zero inside of a zero
              then [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn ]                                 ]
                , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn ], Symb $ Symbol insideOf ZeroTkn ]
                ]
              else []

gram3 = Grammar { startSymbol = Symbol NoTkn NoTkn, fillRule = rule }
