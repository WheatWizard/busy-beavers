module Grammar.Rule4 (gram4) where

{- Here we introduce the idea of zero scopes -}
{- Zero scope is a scope where we know the value cannot be used -}
{- A non-zero scope is a scope where the value *may* be used -}
{- To start we replace our previous rules about being in a <..> with being in a zero scope -}
{- We also say that if we are in a zero scope then {..} is also in a zero scope -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken Bool
  | Delay Symbol
  | Val0

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule (Symbol insideOf rightOf scope) = [
    [ Connect Push [ Symb $         Symbol PushTkn NoTkn False ]                                                    ]
  , [ Connect Push [ Symb $ Delay $ Val0                       ]                                                    ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ], Filled Pop                                        ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn scope ]                                                    ]
  , [ Connect Push [ Symb $         Symbol PushTkn NoTkn False ],             Symb $ Symbol insideOf PushTkn  scope ]
  , [ Connect Push [ Symb $ Delay $ Val0                       ],             Symb $ Symbol insideOf Push0Tkn scope ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ], Filled Pop, Symb $ Symbol insideOf DoubTkn  scope ]
  , [ Connect Loop [ Symb $         Symbol LoopTkn NoTkn scope ],             Symb $ Symbol insideOf LoopTkn  scope ]
  ] ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    pops = if notElem rightOf [ PushTkn, Push0Tkn ] -- Cannot have a pop after a push, use Doub instead
             then [
                 [ Filled Pop                                      ]
               , [ Filled Pop, Symb $ Symbol insideOf PopTkn scope ]
               ]
              else []
    swaps = if rightOf /= SwapTkn -- Cannot have a swap right after a swap
             then [
                 [ Filled Swap                                       ]
               , [ Filled Swap, Symb $ Symbol insideOf SwapTkn scope ]
               ]
              else []
    units = if not scope -- Don't do units if the value doesn't matter
              then [
                  [ Filled Unit                                       ]
                , [ Filled Unit, Symb $ Symbol insideOf UnitTkn scope ]
                ]
               else []
    heights = if not scope -- Don't do heights if the value doesn't matter
                then [
                    [ Filled Height                                         ]
                  , [ Filled Height, Symb $ Symbol insideOf HeightTkn scope ]
                  ]
                 else []
    negates = if not scope -- Don't do a negative if the value doesn't matter
                then [
                    [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ]                                       ]
                  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ], Symb $ Symbol insideOf NegaTkn scope ]
                  ]
                else []
    zeros = if not scope -- Don't do zero if the value doesn't matter
              then [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ]                                       ]
                , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ], Symb $ Symbol insideOf ZeroTkn scope ]
                ]
              else []

gram4 = Grammar { startSymbol = Symbol NoTkn NoTkn False, fillRule = rule }
