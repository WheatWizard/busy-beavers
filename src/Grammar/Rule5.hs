module Grammar.Rule5 (gram5) where

{- Here we make a couple of rules about what can be at the top level of the program -}
{- 1. Our program cannot start with {} (this will always do nothing) -}
{- 2. Our program cannot start with [] (this will alway be zero) -}
{- 3. Our program cannot start with <> (this will do nothing) -}
{- 4. Our program cannot start with (<><>) (this could be expressed as ({}), ([]) or (<>)) -}
{- 5. Our program cannot start with {..} (this loop will be skipped) -}
{- 6. Our program should not have a <..> at the top level. -}
{-    Either its contents are negative in which case [..] is preferable -}
{-    Its contents are positive in which case not using anything is preferable -}
{-    Or its contents are zero in which case there is no reason to use the <..> in the first place -}
{- 7. Our program should not end with <> -}
{- 8. Our program should not end with (..) -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken Bool
  | Delay Symbol
  | Val0

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule (Symbol insideOf rightOf scope) = [
    [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ], Filled Pop                                       ]
  , [ Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ], Filled Pop, Symb $ Symbol insideOf DoubTkn scope ]
  ] ++ loops ++ pushes ++ push0s ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    -- Program should not start with {..}
    loops = if insideOf == NoTkn && rightOf == NoTkn
              then []
              else [
                     [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ] ]
                   , [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ], Symb $ Symbol insideOf LoopTkn scope ]
                   ]
    -- Program should never end in a (<><>)
    terminalPush0s = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Delay $ Val0 ] ] ]
    -- Program should never end in a (..)
    terminalPushes = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ] ] ]
    pushes = if False
                then []
                else terminalPushes ++ [
                       [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ], Symb $ Symbol insideOf PushTkn scope ]
                     ]
    -- Cannot have a push zero as the first thing in the program
    push0s = if (insideOf == NoTkn && rightOf == NoTkn)
                then []
                else terminalPush0s ++ [
                       [ Connect Push [ Symb $ Delay $ Val0 ], Symb $ Symbol insideOf Push0Tkn scope ]
                     ]
 
    -- Cannot have a pop after a push, use Doub instead
    -- Cannot have a pop as the first thing in the program
    pops = if elem rightOf [ PushTkn, Push0Tkn ] || (insideOf == NoTkn && rightOf == NoTkn)
              then []
              else [
                 [ Filled Pop                                      ]
               , [ Filled Pop, Symb $ Symbol insideOf PopTkn scope ]
               ]
    -- No terminal swaps at the top level
    terminalSwaps = if insideOf == NoTkn
                      then []
                      else [ [ Filled Swap ] ]
    -- Cannot have a swap right after a swap
    -- Cannot have a swap as the first thing in the program
    swaps = if rightOf == SwapTkn || (insideOf == NoTkn && rightOf == NoTkn)
             then []
             else terminalSwaps ++ [
                 [ Filled Swap, Symb $ Symbol insideOf SwapTkn scope ]
               ]
    units = if not scope -- Don't do units if the value doesn't matter
              then [
                  [ Filled Unit                                       ]
                , [ Filled Unit, Symb $ Symbol insideOf UnitTkn scope ]
                ]
               else []
    -- Don't do heights if the value doesn't matter
    -- Cannot have a height as the first thing in the program
    heights = if scope || (insideOf == NoTkn && rightOf == NoTkn)
                then []
                else [
                    [ Filled Height                                         ]
                  , [ Filled Height, Symb $ Symbol insideOf HeightTkn scope ]
                  ]
    negates = if not scope -- Don't do a negative if the value doesn't matter
                then [
                    [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ]                                       ]
                  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ], Symb $ Symbol insideOf NegaTkn scope ]
                  ]
                else []
    -- Don't do zero if the value doesn't matter
    -- Don't use a zero at the top level
    zeros = if scope || insideOf == NoTkn
              then []
              else [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ]                                       ]
                , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ], Symb $ Symbol insideOf ZeroTkn scope ]
                ]

gram5 = Grammar { startSymbol = Symbol NoTkn NoTkn False, fillRule = rule }
