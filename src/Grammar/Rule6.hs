module Grammar.Rule6 (gram6) where

{- Here we make a couple of rules about what can come after what -}
{- 1. No {..}{..}. (the second {..} will never be executed) -}
{- 2. No (<><>){..}. ({..} will never be executed) -}
{- 3. No [..][..]. (can always be written as [..] for less) -}
{- 4. No <..><..>. (can always be written as <..> for less) -}
{- 5. No (<><>)(<><>) (can be written as ((<><>)))-}
{- 6. No (..){}(..){} (can always be written as (..){}(..){} for less)-}
{- 7. No {..}{}$ (the pop has value 0)-}
{- 8. No <..>(<><>) (can always be written as (<..>))-}
{- 8. No <>(<><>) (can always be written as (<>))-}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken Bool
  | Delay Symbol
  | Val0

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule (Symbol insideOf rightOf scope) = doubs ++ loops ++ pushes ++ push0s ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    -- We should never have (..){}(..){}
    doubs = if rightOf == DoubTkn
               then []
               else [
                      [
                        Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                      , Filled Pop
                      ]
                    , [
                        Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                      , Filled Pop
                      , Symb $ Symbol insideOf DoubTkn scope
                      ]
                    ]
    -- Program should not start with {..}
    -- We should never have {..}{..}
    -- We should never have (<><>){..}
    loops = if (insideOf == NoTkn && rightOf == NoTkn) || rightOf == LoopTkn || rightOf == Push0Tkn
              then []
              else [
                     [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ] ]
                   , [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ], Symb $ Symbol insideOf LoopTkn scope ]
                   ]
    -- Program should never end in a (<><>)
    terminalPush0s = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Delay $ Val0 ] ] ]
    -- Program should never end in a (..)
    terminalPushes = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ] ] ]
    pushes = if False
                then []
                else terminalPushes ++ [
                       [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ], Symb $ Symbol insideOf PushTkn scope ]
                     ]
    -- Cannot have a push zero as the first thing in the program
    -- We should never have (<><>)(<><>)
    -- We should never have <>(<><>)
    -- We should never have <..>(<><>)
    push0s = if (insideOf == NoTkn && rightOf == NoTkn) || rightOf == Push0Tkn || rightOf == ZeroTkn || rightOf == SwapTkn
                then []
                else terminalPush0s ++ [
                       [ Connect Push [ Symb $ Delay $ Val0 ], Symb $ Symbol insideOf Push0Tkn scope ]
                     ]
    -- We should never have {..}{}$
    terminalPops = if (insideOf == NoTkn && rightOf == LoopTkn) 
                     then []
                     else [ [ Filled Pop ] ]
    -- Cannot have a pop after a push, use Doub instead
    -- Cannot have a pop as the first thing in the program
    pops = if elem rightOf [ PushTkn, Push0Tkn ] || (insideOf == NoTkn && rightOf == NoTkn)
              then []
              else terminalPops ++ [
                 [ Filled Pop, Symb $ Symbol insideOf PopTkn scope ]
               ]
    -- No terminal swaps at the top level
    terminalSwaps = if insideOf == NoTkn
                      then []
                      else [ [ Filled Swap ] ]
    -- Cannot have a swap right after a swap
    -- Cannot have a swap as the first thing in the program
    swaps = if rightOf == SwapTkn || (insideOf == NoTkn && rightOf == NoTkn)
             then []
             else terminalSwaps ++ [
                 [ Filled Swap, Symb $ Symbol insideOf SwapTkn scope ]
               ]
    units = if not scope -- Don't do units if the value doesn't matter
              then [
                  [ Filled Unit                                       ]
                , [ Filled Unit, Symb $ Symbol insideOf UnitTkn scope ]
                ]
               else []
    -- Don't do heights if the value doesn't matter
    -- Cannot have a height as the first thing in the program
    heights = if scope || (insideOf == NoTkn && rightOf == NoTkn)
                then []
                else [
                       [ Filled Height                                         ]
                     , [ Filled Height, Symb $ Symbol insideOf HeightTkn scope ]
                     ]
    -- Don't do [..] if the value doesn't matter
    -- We should never have [..][..]
    negates = if scope || rightOf == NegaTkn
                then []
                else [
                       [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ]                                       ]
                     , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ], Symb $ Symbol insideOf NegaTkn scope ]
                     ]
    -- Don't do <..> if the value doesn't matter
    -- Don't use <..> at the top level
    -- We should never have <..><..>
    zeros = if scope || insideOf == NoTkn || rightOf == ZeroTkn
              then []
              else [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ]                                       ]
                , [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ], Symb $ Symbol insideOf ZeroTkn scope ]
                ]

gram6 = Grammar { startSymbol = Symbol NoTkn NoTkn False, fillRule = rule }
