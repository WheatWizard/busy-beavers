module Grammar.Rule7 (gram7) where

{- Here we make a couple of rules about what can go inside of what -}
{- 1. No [..[..]..] (can always be written as [..]..[..] )-}
{- 2. No {{..}} (can always be written as {..} )-}

{- 3. No (<..>..){} (can always be written as <..>(..){} )-}
{- 4. No (..<..>){} (can always be written as (..){}<..> )-}
{- 5. No [<..>..] (can always be written as <..>[..] )-}
{- 6. No [..<..>] (can always be written as [..]<..> )-}
{- 7. No (<..>..){} (can always be written as <..>(..){} )-}
{- 8. No (..<..>){} (can always be written as (..){}<..> )-}

{- 9. No <(<><>)..> (can always be written as (<><>)<..> )-}
{- 10. No <..(<><>)> (can always be written as (<..>) )-}
{- 11. No [(<><>)..] (can always be written as (<><>)[..] )-}
{- 12. No [..(<><>)] (can always be written as [..](<><>) )-}
{- 13. No ((<><>)..){} (can always be written as (<><>)(..){} )-}
{- 14. No (..(<><>)){} (can always be written as (..){}(<><>) )-}

{- 15. No <<>..> (can always be written as <><..> )-}
{- 16. No <..<>> (can always be written as <..><> )-}
{- 17. No [<>..] (can always be written as <>[..] )-}
{- 18. No [..<>] (can always be written as [..]<> )-}
{- 19. No (<>..){} (can always be written as <>(..){} )-}
{- 20. No (..<>){} (can always be written as (..){}<> )-}

{- 21. No ([..]){} (can always be written as [(..){}]) -}
{- 22. No {[..]} (can always be written as [{..}]) -}
{- 23. No {<..>} (can always be written as <{..}>)-}
{- 24. No {(..){}} (can always be written as ({..}){})-}

{- I also forgot to prevent (..){} in a zero scope so I added that. -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | HeightTkn
  | UnitTkn
  | SwapTkn
  | NoTkn
  deriving Eq

data Symbol =
    Symbol BFToken BFToken Bool
  | Delay Symbol
  | Val0

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule (Symbol insideOf rightOf scope) = doubs ++ loops ++ pushes ++ push0s ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    -- We should never have {(..){}}
    terminalDoubs = if (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [
                          [
                            Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                          , Filled Pop
                          ]
                        ]
    -- Double should not be in a zero scope
    -- We should never have (..){}(..){}
    doubs = if rightOf == DoubTkn || scope
               then []
               else terminalDoubs ++ [
                      [
                        Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                      , Filled Pop
                      , Symb $ Symbol insideOf DoubTkn scope
                      ]
                    ]
    -- We should never have {{..}}
    terminalLoops = if (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [ [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ] ] ]
    -- Program should not start with {..}
    -- We should never have {..}{..}
    -- We should never have (<><>){..}
    loops = if (insideOf == NoTkn && rightOf == NoTkn) || rightOf == LoopTkn || rightOf == Push0Tkn
              then []
              else terminalLoops
                ++ [ [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ], Symb $ Symbol insideOf LoopTkn scope ] ]
    -- Program should never end in a (<><>)
    -- We should never have <..(<><>)>
    -- We should never have [..(<><>)]
    -- We should never have (..(<><>)){}
    terminalPush0s = if elem insideOf [NoTkn, ZeroTkn, NegaTkn, DoubTkn]
                       then []
                       else [ [ Connect Push [ Symb $ Delay $ Val0 ] ] ]
    -- Program should never end in a (..)
    terminalPushes = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ] ] ]
    pushes = if False
                then []
                else terminalPushes ++ [
                       [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ], Symb $ Symbol insideOf PushTkn scope ]
                     ]
    -- Cannot have a push zero as the first thing in the program
    -- We should never have (<><>)(<><>)
    -- We should never have <>(<><>)
    -- We should never have <..>(<><>)
    -- We should never have <(<><>)..>
    -- We should never have [(<><>)..]
    -- We should never have ((<><>)..){}
    push0s = if rightOf == Push0Tkn
             || rightOf == ZeroTkn
             || rightOf == SwapTkn
             || (rightOf == NoTkn && elem insideOf [NoTkn, ZeroTkn, NegaTkn, DoubTkn])
                then []
                else terminalPush0s ++ [
                       [ Connect Push [ Symb $ Delay $ Val0 ], Symb $ Symbol insideOf Push0Tkn scope ]
                     ]
    -- We should never have {..}{}$
    terminalPops = if (insideOf == NoTkn && rightOf == LoopTkn) 
                     then []
                     else [ [ Filled Pop ] ]
    -- Cannot have a pop after a push, use Doub instead
    -- Cannot have a pop as the first thing in the program
    pops = if elem rightOf [ PushTkn, Push0Tkn ] || (insideOf == NoTkn && rightOf == NoTkn)
              then []
              else terminalPops ++ [
                 [ Filled Pop, Symb $ Symbol insideOf PopTkn scope ]
               ]
    -- No terminal swaps at the top level
    -- We should never have <..<>>
    -- We should never have [..<>]
    -- We should never have (..<>){}
    terminalSwaps = if elem insideOf [ NoTkn, ZeroTkn, NegaTkn ]
                      then []
                      else [ [ Filled Swap ] ]
    -- Cannot have a swap right after a swap
    -- Cannot have a swap as the first thing in the program
    -- We should never have <<>..>
    -- We should never have [<>..]
    -- We should never have (<>..){}
    swaps = if rightOf == SwapTkn
            || (rightOf == NoTkn && elem insideOf [ NoTkn, ZeroTkn, NegaTkn, DoubTkn ])
             then []
             else terminalSwaps ++ [
                 [ Filled Swap, Symb $ Symbol insideOf SwapTkn scope ]
               ]
    units = if not scope -- Don't do units if the value doesn't matter
              then [
                  [ Filled Unit                                       ]
                , [ Filled Unit, Symb $ Symbol insideOf UnitTkn scope ]
                ]
               else []
    -- Don't do heights if the value doesn't matter
    -- Cannot have a height as the first thing in the program
    heights = if scope || (insideOf == NoTkn && rightOf == NoTkn)
                then []
                else [
                    [ Filled Height                                         ]
                  , [ Filled Height, Symb $ Symbol insideOf HeightTkn scope ]
                  ]
    -- We should never have {[..]}
    -- We should never have ([..]){}
    terminalNegates = if (rightOf == NoTkn && elem insideOf [ LoopTkn, DoubTkn ])
                        then []
                        else [ [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ] ] ]
    -- Don't do [..] if the value doesn't matter
    -- We should never have [..][..]
    -- We should never have [..[..]..]
    negates = if scope || rightOf == NegaTkn || insideOf == NegaTkn
                then []
                else [
                    [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ]                                       ]
                  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ], Symb $ Symbol insideOf NegaTkn scope ]
                  ]
    -- We should never have (..<..>){}
    -- We should never have [..<..>]
    -- We should never have {<..>}
    terminalZeros = if insideOf == DoubTkn
                    || insideOf == NegaTkn
                    || (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [ [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ] ] ]
    -- Don't do <..> if the value doesn't matter
    -- Don't use <..> at the top level
    -- We should never have <..><..>
    -- We should never have (<..>..){}
    -- We should never have [<..>..]
    zeros = if scope
            || insideOf == NoTkn
            || rightOf == ZeroTkn
            || (insideOf == DoubTkn && rightOf == NoTkn)
            || (insideOf == NegaTkn && rightOf == NoTkn)
              then []
              else terminalZeros ++ [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ], Symb $ Symbol insideOf ZeroTkn scope ]
                ]

gram7 = Grammar { startSymbol = Symbol NoTkn NoTkn False, fillRule = rule }
