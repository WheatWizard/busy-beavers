module Grammar.Rule9 (gram9) where

{- Here we make a couple of rules about [] -}
{- 2. No [][][][] (can always be written as ([][]){} )-}
{- 3. No {ab} where a is only ()s and b is only []s (these will either never be executed or never halt) -}
{- 4. No [][](..){} (can always be written as ([]..){} ) -}
{- 5. No (..){}[][] (can always be written as (..[]){} ) -}

import Grammar

import Program

data BFToken =
    PushTkn
  | ZeroTkn
  | NegaTkn
  | LoopTkn
  | DoubTkn
  | Push0Tkn
  | PopTkn
  | H1Tkn
  | H2Tkn
  | H3Tkn
  | OneTkn
  | TwoTkn
  | ThreeTkn
  | SwapTkn
  | NoTkn
  deriving Eq

begins :: BFToken -> Bool
begins NoTkn = True
begins OneTkn = True
begins TwoTkn = True
begins ThreeTkn = True
begins _ = False

data Symbol =
    Symbol BFToken BFToken Bool
  | Delay Symbol
  | Val0
  | Val1
  | ValH

rule :: Symbol -> [ [ Structure Symbol Program ] ]
rule (Delay s) = [ [ Symb s ] ]
rule Val0 = [ [ Filled Swap, Filled Swap] ] 
rule Val1 = [ [ Filled Unit ] ] 
rule ValH = [ [ Filled Height ] ] 
rule (Symbol insideOf rightOf scope) = doubs ++ loops ++ pushes ++ push0s ++ pops ++ swaps ++ units ++ heights ++ negates ++ zeros
  where
    -- We should never have {(..){}}
    terminalDoubs = if (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [
                          [
                            Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                          , Filled Pop
                          ]
                        ]
    -- Double should not be in a zero scope
    -- We should never have (..){}(..){}
    -- We should never have ()()(..){}
    doubs = if elem rightOf [ DoubTkn, TwoTkn, ThreeTkn, H2Tkn, H3Tkn ] || scope
               then []
               else terminalDoubs ++ [
                      [
                        Connect Push [ Symb $ Delay $ Symbol DoubTkn NoTkn False ]
                      , Filled Pop
                      , Symb $ Symbol insideOf DoubTkn scope
                      ]
                    ]
    -- We should never have {{..}}
    terminalLoops = if (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [ [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ] ] ]
    -- Program should not start with a{..} where a is some number of ()s
    -- We should never have {..}{..}
    -- We should never have (<><>){..}
    loops = if (insideOf == NoTkn && begins rightOf) || rightOf == LoopTkn || rightOf == Push0Tkn
              then []
              else terminalLoops
                ++ [ [ Connect Loop [ Symb $ Symbol LoopTkn NoTkn scope ], Symb $ Symbol insideOf LoopTkn scope ] ]
    -- Program should never end in a (<><>)
    -- We should never have <..(<><>)>
    -- We should never have [..(<><>)]
    -- We should never have (..(<><>)){}
    terminalPush0s = if elem insideOf [NoTkn, ZeroTkn, NegaTkn, DoubTkn]
                       then []
                       else [ [ Connect Push [ Symb $ Delay $ Val0 ] ] ]
    -- Program should never end in a (..)
    terminalPushes = if insideOf == NoTkn
                       then []
                       else [ [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ] ] ]
    pushes = if False
                then []
                else terminalPushes ++ [
                       [ Connect Push [ Symb $ Symbol PushTkn NoTkn False ], Symb $ Symbol insideOf PushTkn scope ]
                     ]
    -- Cannot have a push zero as the first thing in the program
    -- We should never have (<><>)(<><>)
    -- We should never have <>(<><>)
    -- We should never have <..>(<><>)
    -- We should never have <(<><>)..>
    -- We should never have [a(<><>)..] where a is some number of ()s
    -- We should never have (a(<><>)..){} where a is some number of ()s
    push0s = if rightOf == Push0Tkn
             || rightOf == ZeroTkn
             || rightOf == SwapTkn
             || (begins rightOf && elem insideOf [ NoTkn, ZeroTkn, NegaTkn, DoubTkn ])
                then []
                else terminalPush0s ++ [
                       [ Connect Push [ Symb $ Delay $ Val0 ], Symb $ Symbol insideOf Push0Tkn scope ]
                     ]
    -- We should never have {..}{}$
    terminalPops = if (insideOf == NoTkn && rightOf == LoopTkn) 
                     then []
                     else [ [ Filled Pop ] ]
    -- Cannot have a pop after a push, use Doub instead
    -- Cannot have a pop as the first thing in the program
    pops = if elem rightOf [ PushTkn, Push0Tkn ] || (insideOf == NoTkn && begins rightOf)
              then []
              else terminalPops ++ [
                 [ Filled Pop, Symb $ Symbol insideOf PopTkn scope ]
               ]
    -- No terminal swaps at the top level
    -- We should never have <..<>>
    -- We should never have [..<>]
    -- We should never have (..<>){}
    terminalSwaps = if elem insideOf [ NoTkn, ZeroTkn, NegaTkn ]
                      then []
                      else [ [ Filled Swap ] ]
    -- Cannot have a swap right after a swap
    -- Cannot have a swap as the first thing in the program
    -- We should never have <<>..>
    -- We should never have [<>..]
    -- We should never have (<>..){}
    swaps = if rightOf == SwapTkn
            || (begins rightOf && elem insideOf [ NoTkn, ZeroTkn, NegaTkn, DoubTkn ])
             then []
             else terminalSwaps ++ [
                 [ Filled Swap, Symb $ Symbol insideOf SwapTkn scope ]
               ]
    -- We should never have {a} where a is only ()s
    terminalUnits = if insideOf == LoopTkn
                      then []
                      else [
                          [ Filled Unit                                 ]
                        , [ Filled Unit, Symb Val1                      ]
                        , [ Filled Unit, Filled Unit, Symb $ Delay Val1 ]
                        ]
    -- Don't do units if the value doen't matter
    -- We should never have a() where a is not ()s
    -- We should never have ()()()()
    units = if scope
            || rightOf /= NoTkn
              then []
              else terminalUnits ++ [
                  [ Filled Unit,                                 Symb $ Symbol insideOf OneTkn   scope ]
                , [ Filled Unit, Symb Val1,                      Symb $ Symbol insideOf TwoTkn   scope ]
                , [ Filled Unit, Filled Unit, Symb $ Delay Val1, Symb $ Symbol insideOf ThreeTkn scope ]
                ]
    -- We should never have {ab} where a is only ()s and b is only []s
    terminalHeights = if (insideOf == LoopTkn && begins rightOf)
                      then []
                      else [
                          [ Filled Height                                 ]
                        , [ Filled Height, Symb ValH                      ]
                        , [ Filled Height, Filled Height, Symb $ Delay ValH ]
                        ]
    -- Don't do heights if the value doesn't matter
    -- We should never have ^a[] where a is only ()s
    -- We should never have [][][][]
    -- We should never have (..){}[][]
    heights = if scope
            || (insideOf == NoTkn && begins rightOf)
            || elem rightOf [ H1Tkn, H2Tkn, H3Tkn ]
              then []
              else if rightOf == DoubTkn
                     then take 1 terminalHeights ++ [ [ Filled Height, Symb $ Symbol insideOf H1Tkn scope ] ]
                     else terminalHeights ++ [
                       [ Filled Height,                                   Symb $ Symbol insideOf H1Tkn scope ]
                     , [ Filled Height, Symb ValH,                        Symb $ Symbol insideOf H2Tkn scope ]
                     , [ Filled Height, Filled Height, Symb $ Delay ValH, Symb $ Symbol insideOf H3Tkn scope ]
                     ]
    -- We should never have {[..]}
    -- We should never have ([..]){}
    terminalNegates = if (rightOf == NoTkn && elem insideOf [ LoopTkn, DoubTkn ])
                        then []
                        else [ [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ] ] ]
    -- Don't do [..] if the value doesn't matter
    -- We should never have [..][..]
    -- We should never have [..[..]..]
    negates = if scope || rightOf == NegaTkn || insideOf == NegaTkn
                then []
                else [
                    [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ]                                       ]
                  , [ Connect Nega [ Symb $ Symbol NegaTkn NoTkn True ], Symb $ Symbol insideOf NegaTkn scope ]
                  ]
    -- We should never have (..<..>){}
    -- We should never have [..<..>]
    -- We should never have {<..>}
    terminalZeros = if insideOf == DoubTkn
                    || insideOf == NegaTkn
                    || (insideOf == LoopTkn && rightOf == NoTkn)
                      then []
                      else [ [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ] ] ]
    -- Don't do <..> if the value doesn't matter
    -- Don't use <..> at the top level
    -- We should never have <..><..>
    -- We should never have (<..>..){}
    -- We should never have [<..>..]
    zeros = if scope
            || insideOf == NoTkn
            || rightOf == ZeroTkn
            || (insideOf == DoubTkn && begins rightOf)
            || (insideOf == NegaTkn && begins rightOf)
              then []
              else terminalZeros ++ [
                  [ Connect Zero [ Symb $ Symbol ZeroTkn NoTkn True ], Symb $ Symbol insideOf ZeroTkn scope ]
                ]

gram9 = Grammar { startSymbol = Symbol NoTkn NoTkn False, fillRule = rule }
