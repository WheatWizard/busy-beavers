module Main where

import Grammar

import Grammar.Rule9

import RunBrainFlak

import Program

import System.Environment

printRecords :: Integer -> [(Maybe Integer, [Program])] -> IO ()
printRecords = go 0 0
  where
    go a b bound [] = do
      putStrLn $ show a ++ " halted, " ++ show b ++ " did not." 
      putStr $ "BB(n) is at least " ++ show bound
    go a b bound ((Nothing, progs) : rest) = do
      putStr "??? , "
      mapM_ (putStr . show) progs
      putStr "\n"
      go a (b + 1) bound rest
    go a b bound ((Just n, progs) : rest)
      | bound >  n = go (a + 1) b bound rest
      | bound <= n = do
        putStr $ show n ++ " , "
        mapM_ (putStr . show) progs
        putStr "\n"
        go a b n rest

printAll :: Integer -> [(Maybe Integer, [Program])] -> IO ()
printAll = go 0 0
  where
    go a b bound [] = do
      putStrLn $ show a ++ " halted, " ++ show b ++ " did not." 
      putStr $ "BB(n) is at least " ++ show bound
    go a b bound ((Nothing, progs) : rest) = do
      putStr "??? , "
      mapM_ (putStr . show) progs
      putStr "\n"
      go a (b + 1) bound rest
    go a b bound ((Just n, progs) : rest)
      | bound >  n = do
        putStr $ show n ++ " , "
        mapM_ (putStr . show) progs
        putStr "\n"
        go (a + 1) b bound rest
      | bound <= n = do
        putStr $ show n ++ " , "
        mapM_ (putStr . show) progs
        putStr "\n"
        go a b n rest

main :: IO ()
main = do
  (first : _) <- getArgs
  let size = read first 
  printRecords 0 $ map (\x -> (run 10 x startState, x)) $ fillGram gram9 size
