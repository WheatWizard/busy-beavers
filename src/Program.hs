module Program (Program(..)) where

data Program
  = Unit
  | Height
  | Swap
  | Pop
  | Push [Program]
  | Nega [Program]
  | Zero [Program]
  | Loop [Program]

instance Show Program where
  show ( Unit    ) = "()"
  show ( Height  ) = "[]"
  show ( Swap    ) = "<>"
  show ( Pop     ) = "{}"
  show ( Push ps ) = "(" ++ ( show =<< ps ) ++ ")"
  show ( Nega ps ) = "[" ++ ( show =<< ps ) ++ "]"
  show ( Zero ps ) = "<" ++ ( show =<< ps ) ++ ">"
  show ( Loop ps ) = "{" ++ ( show =<< ps ) ++ "}"

