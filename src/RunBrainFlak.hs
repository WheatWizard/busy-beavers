module RunBrainFlak where

import Program

type Stack = [ Integer ]

data State = State Stack Stack Bool

pop :: State -> (State, Integer)
pop (State a b True)
 | (popped : unpopped) <- a = (State unpopped b True,  popped)
 | otherwise                = (State []       b True,  0)
pop (State a b False)
 | (popped : unpopped) <- b = (State a unpopped False, popped)
 | otherwise                = (State a []       False, 0)

push :: Integer -> State -> State
push n (State a b act)
  | act       = State (n : a) b act
  | otherwise = State a (n : b) act

height :: State -> Integer
height (State a b act)
  | act       = toInteger $ length a
  | otherwise = toInteger $ length b

run :: Int -> [Program] -> State -> Maybe Integer
run = ((fmap snd .) .) . flip go 0
  where
    go :: Int -> Integer -> [Program] -> State -> Maybe (State, Integer)
    go _ n [] s = Just (s, n)
    go 0 _ _ _  = Nothing
    go cL n (Unit   : rest) s = go cL (n + 1) rest s
    go cL n (Height : rest) s = go cL (n + height s) rest s
    go cL n (Pop    : rest) s = go cL (n + val) rest nextS
      where (nextS, val) = pop s
    go cL n (Swap   : rest) (State a b act) = go cL n rest (State a b $ not act) 
    go cL n (Push progs : rest) s = case go cL 0 progs s of
      Just (nextS, val) -> go cL (n + val) rest (push val nextS)
      Nothing -> Nothing
    go cL n (Nega progs : rest) s = case go cL 0 progs s of
      Just (nextS, val) -> go cL (n - val) rest nextS
      Nothing -> Nothing
    go cL n (Zero progs : rest) s = case go cL 0 progs s of
      Just (nextS, _)   -> go cL n rest nextS
      Nothing -> Nothing
    go cL n (Loop progs : rest) s 
      | top == 0  = go cL n rest s
      | otherwise = case go (cL - 1) n progs s of
        Just (nextS, nextN) -> go (cL - 1) nextN (Loop progs : rest) nextS
        Nothing -> Nothing
      where
        (_, top) = pop s

startState :: State
startState = State [] [] True
